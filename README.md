# VeriMake CH549DB

版本：v1.0.0

## 介绍

教程： [以 CH549 为例的 51 教程 | 主索引](https://verimake.com/d/19-ch549-51)  
原理图 ： [VeriMake_CH549DB_v1.0.0](./doc/Schematic_VeriMake_CH549DB_v1_0_0_20220123.pdf)  
Fritzing 元件 ： [Verimake CH549EVB (V0.31).fzpz](./doc/Verimake%20CH549EVB%20(V0.31).fzpz)  

VeriMake 论坛有专门的 [***CH549 版块***](https://verimake.com/t/CH549) 供开发者发帖展示相关项目、分享开发经验、提出开发过程中遇到问题等。  
相关硬件可以从 [***VeriMake的淘宝店***](https://verimake.taobao.com/) 购买。  
![***二维码***](./doc/pic/BiliTao-QR-Code-0.15-fillet.jpg)

## 例程

例程在 `firmware` 文件夹下

| 项目名                   | 项目内容                                           |
| ------------------------ | -------------------------------------------------- |
| TouchKey                 | 沁恒触摸按键例程                                   |
| TouchKeyMeasureErrorTest | 测试电容按键充电时间、电压采样值、采样值方差的关系 |
| WaterLevelMeasuring      | 水位测量程序                                       |
| lib                      | 所有项目共用的库                                   |

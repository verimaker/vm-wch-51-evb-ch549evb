/***************************Encoding = UTF-8***********************************
 * WaterLevelMeasuring
 * main.c
 * 
 * 水位测量程序
 * 在OLED屏上输出电压采样值和水位
 * 使用 SSD1306 OLED 屏，接线如下
 * 
 * 水位测量引脚 - P1.0
 * 屏幕的接法见 CH549_OLED.h - //SSD1306的SPI接口定义
 * 
 * 使用方法：
 * 1.调节 WL_CPW 使最低水位的电压采样值在 3500 左右
 * 2.容器装满水，记下电压采样值
 * 3.修改WL_MIN_VAL，WL_MAX_VAL，WL_MAX，WL_MIN，WL_UNIT
 * 
 * 教程链接： 
 * 
 * 适用于 VeriMake CH549DB 1.x.x
 * 主频：48MHz
 * -- by Benue@Verimake
*******************************************************************************/
#include <CH549_OLED.h>    
#include <CH549_SPI.h> 
#include <CH549_ADC.h>
#include <CH549_DEBUG.h>
#include <CH549_sdcc.h>


#define WL_MAX 5.0		//Maximun water level,最高水位
#define WL_MAX_VAL 3264	//最高水位对应的电压采样值
#define WL_MIN 0.0		//Minimun water level,最低水位
#define WL_MIN_VAL 3658	//最低水位对应的电压采样值
#define WL_UNIT ("mL")	//Unit of water level,水位的单位
#define WL_CH 0			//Measuring channel, 测量用的 ADC 通道
#define WL_PIN ( P1_0 )	//Measuring Pin,测量用的引脚，应与 ADC 通道相匹配
#define WL_CPW 80		//Pulse width for charging the touch key pin，in 2/Fsys，充电时间，单位为2个系统时钟周期

int oled_colum;
int oled_row;
void setCursor(int column,int row);

//chargePulseWidth : Pulse width for charging the touch key pin from low to high,in 2/Fsys
int touchKey( UINT8 ch,UINT8 chargePulseWidth )
{
    ADC_CHAN = ADC_CHAN & (~MASK_ADC_CHAN) | ch;     //外部通道选择
    //电容较大时可以先设置IO低，然后恢复浮空输入实现手工放电，≤0.2us
    TKEY_CTRL = chargePulseWidth;                         //充电脉冲宽度配置，仅低7位有效（同时清除bADC_IF，启动一次TouchKey）
    while(ADC_CTRL&bTKEY_ACT);
	int ret = (ADC_DAT&0xFFF);
    return ret;
}

void WLM_init( UINT8 ch )
{
	//Touch采样通道设置为高阻输入
    if(ch<=7)                                  //P10~P17引脚配置
    {
        P1_MOD_OC &= ~(1<<ch);                 //高阻输入
        P1_DIR_PU &= ~(1<<ch);
    }
    if(ch>7 && ch<=0x0f)                       //P00~P07引脚配置
    {
        P0_MOD_OC &= ~(1<<(ch-8));             //高阻输入
        P0_DIR_PU &= ~(1<<(ch-8));
    }

    ADC_CFG |= (bADC_EN|bADC_AIN_EN);                //开启ADC模块电源,选择外部通道
    ADC_CFG = ADC_CFG & ~(bADC_CLK0 | bADC_CLK1);    //选择ADC参考时钟
    ADC_CHAN = (3<<4);                               //默认选择外部通道0
    ADC_CTRL = bADC_IF;                              //清除ADC转换完成标志，写1清零
}

// 把电压采样值 转为 水位
float WLM_val2level(int val){
	float waterLevel;
	int i;
	if(val > WL_MIN_VAL) return 0;

	if(val > WL_MAX_VAL){ 
		float estimate = (float)(WL_MAX - WL_MIN) * (val - WL_MIN_VAL)/(WL_MAX_VAL - WL_MIN_VAL);
		waterLevel = WL_MIN + estimate;
		return waterLevel;
	}
	return WL_MAX;
}

int samples[100];

void main()
{
	int i;
    CfgFsys( );           //CH549时钟选择配置
    mDelaymS(20);
	CH549UART1Init();
	
	SPIMasterModeSet(3);  //SPI主机模式设置，模式3
    SPI_CK_SET(12);       //设置SPI sclk 时钟信号为12分频
	OLED_Init();		  //初始化OLED  
	OLED_Clear();         //将OLED屏幕上内容清除
	setFontSize(16);      //设置文字大小
    WLM_init(WL_CH);

	OLED_Clear();
	while(1){
		long val;

		setCursor(0,0);
		printf_fast_f("Water Level");

		for(int j = 0;j<20000;j++){
			val += touchKey(WL_CH,WL_CPW);
		}
		val /= 20000;

		setCursor(0,2);
		printf_fast_f("Level: %.1f%s",WLM_val2level(val),WL_UNIT);
		setCursor(0,4);
		printf_fast_f("Value: %ld",val);
		mDelaymS(200);
	}
}


/********************************************************************
* 函 数 名       : putchar
* 函数功能       : 将printf映射到OLED屏幕输出上
* 输    入      : 字符串
* 输    出    	: 字符串
********************************************************************/
int putchar( int a)
{      
    //在光标处显示文字 a
    OLED_ShowChar(oled_colum,oled_row,a);
    //将光标右移一个字的宽度(8列),以显示下一个字
    oled_colum+=8;
    
    /*当此行不足以再显示一个字时，换行.
    即行坐标+2(下移1个字的高度,2行=16像素).
    同时光标回到最边(列坐标=0).
    */
    if (oled_colum>120){oled_colum=0;oled_row+=2;}
    return(a);
}

/********************************************************************
* 函 数 名       : setCursor
* 函数功能		   : 设置光标（printf到屏幕上的字符串起始位置）
* 输    入       : 行坐标 列坐标(此处一行为8个像素，一列为1个像素,所以屏幕上共有8行128列)
* 输    出    	 : 无
********************************************************************/
void setCursor(int column,int row)
{
    oled_colum = column;
    oled_row = row;
}
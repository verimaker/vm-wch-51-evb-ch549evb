/***************************Encoding = UTF-8*************************************
 * TouchKeyMeasureErrorTest
 * main.c
 * 测试电容按键充电时间、电压采样值、采样值方差的关系
 * 从 UART1 输出，接线如下
 * P2.7 - TXD1
 * P2.6 - RXD1
 * 
 * 适用于 VeriMake CH549DB 1.x.x
 * -- by Benue@Verimake
*******************************************************************************/

#include <CH549_UART.h>
#include <CH549_ADC.h>
#include <CH549_DEBUG.h>
#include <CH549_sdcc.h>

//chargePulseWidth : Pulse width for charging the touch key pin from low to high,in 2/Fsys
int touchKey( UINT8 ch,UINT8 chargePulseWidth )
{
    ADC_CHAN = ADC_CHAN & (~MASK_ADC_CHAN) | ch;     //外部通道选择
    //电容较大时可以先设置IO低，然后恢复浮空输入实现手工放电，≤0.2us
    TKEY_CTRL = chargePulseWidth;                         //充电脉冲宽度配置，仅低7位有效（同时清除bADC_IF，启动一次TouchKey）
    while(ADC_CTRL&bTKEY_ACT);
	int ret = (ADC_DAT&0xFFF);
    return ret;
}

void WLM_init( UINT8 ch )
{
	//Touch采样通道设置为高阻输入
    if(ch<=7)                                  //P10~P17引脚配置
    {
        P1_MOD_OC &= ~(1<<ch);                 //高阻输入
        P1_DIR_PU &= ~(1<<ch);
    }
    if(ch>7 && ch<=0x0f)                       //P00~P07引脚配置
    {
        P0_MOD_OC &= ~(1<<(ch-8));             //高阻输入
        P0_DIR_PU &= ~(1<<(ch-8));
    }

    ADC_CFG |= (bADC_EN|bADC_AIN_EN);                //开启ADC模块电源,选择外部通道
    ADC_CFG = ADC_CFG & ~(bADC_CLK0 | bADC_CLK1);    //选择ADC参考时钟
    ADC_CHAN = (3<<4);                               //默认选择外部通道0
    ADC_CTRL = bADC_IF;                              //清除ADC转换完成标志，写1清零
}


int samples[100];
void main()
{
    CfgFsys( );                                                                //CH549时钟选择配置
    mDelaymS(20);
	CH549UART1Init();
    printf("Water Level Measuring demo ...\n");
    WLM_init(4);

	int j = 0;
	long avr,sqerr;
    while(j<128)
    {
		avr = 0;
		int i;
		for(i =0 ; i<100 ; i++){
			samples[i] = touchKey(4,j);
			avr += samples[i];
		}
		avr /= 100;
		sqerr = 0;
		printf("CPW : %d , ", j);
		printf("Average : %lu , ",avr);
		for(i =0 ; i<100 ; i++){
			long err = samples[i] - (int)avr ;
			sqerr += err * err;
		}
		sqerr /= 100;
		printf("SqErr : %lu .\n ",sqerr);
		mDelaymS(2);
		j++;
	}
	while(1);
}
int putchar( int a)
{      
   CH549UART1SendByte(a);                                                     //printf映射到串口1输出
   return(a);
}